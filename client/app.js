import React from 'react';
import {Panel} from './mainPanel';
import {DEPARTMENTS_TYPE, POSITIONS_TYPE} from './constans';
import Dispatcher from './store';
import {typeIdToStr, getCurrentPanelConfigByHash} from './utils';

const DISPATCHER = new Dispatcher();

export default class App extends React.Component {

    constructor(props) {
        super(props);
        const config = getCurrentPanelConfigByHash()
        this.state = {
            typeId: config.typeId,
            itemId: config.itemId
        };
        this.changeHeaderSelection = this.changeHeaderSelection.bind(this);
    }

    changeHeaderSelection(newSelectedId) {
        this.setState(Object.assign({}, this.state, {typeId: newSelectedId, itemId: null}));
    }

    render() {
        return (
            <div id="app">
                <Header typeId={this.state.typeId} onChange={this.changeHeaderSelection}/>
                <Panel typeId={this.state.typeId} itemId={this.state.itemId} dispatcher={DISPATCHER}/>
            </div>
        );
    }
}

class Header extends React.Component {

    constructor(props){
        super(props);
        this.changePanel = this.changePanel.bind(this);
        this.state = {selectedId: props.typeId};
    }

    changePanel(newPanelType) {
        this.setState({
            selectedId: newPanelType
        });
        this.props.onChange(newPanelType);
    }

    render() {
        return (
            <nav className="navbar navbar-default navbar-static-top">
                <div className="container">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#departments">Vacancies SPA</a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav">
                            <li className={this.state.selectedId === DEPARTMENTS_TYPE ? "active" : ""}>
                                <a href="#departments"
                                   onClick={() => this.changePanel(DEPARTMENTS_TYPE)}>Departments</a>
                            </li>
                            <li className={this.state.selectedId === POSITIONS_TYPE ? "active" : ""}>
                                <a href="#positions"
                                   onClick={() => this.changePanel(POSITIONS_TYPE)}>Positions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}
