import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import {DEPARTMENTS_TYPE} from './constans'


ReactDOM.render(
  <App typeId={DEPARTMENTS_TYPE}/>,
  document.getElementById('root')
);
