import React from 'react';
import Datetime from 'react-datetime';
import './css/datepicker.css';
import {
    DEPARTMENTS_TYPE, POSITIONS_TYPE, DEPARTMENT_TYPE, POSITION_TYPE,
    VACANCY_TYPE, EMPLOYEE_TYPE, EMPLOYEES_TYPE, VACANCIES_TYPE
} from './constans';
import {typeIdToStr} from './utils';

export class Panel extends React.Component {

    constructor(props) {
        super(props);
        this.handleCreateNew = this.handleCreateNew.bind(this);
        this.handleMoveTo = this.handleMoveTo.bind(this);
        this.getComponentByState = this.getComponentByState.bind(this);
        this.dispatcher = props.dispatcher;
        this.state = {typeId: props.typeId, itemId: props.itemId, data: []};
    }

    componentWillMount() {
        const callback = (state) => this.setState(state);
        if(this.state.typeId === DEPARTMENTS_TYPE || this.state.typeId === POSITIONS_TYPE) {
            this.dispatcher.dispatch({typeId: this.state.typeId, action: 'loadAll'}, this.state, callback);
        }
    }

    componentWillReceiveProps(nextProps) {
        const newState0 = Object.assign({}, {typeId: nextProps.typeId, data: []});
        const callback = (state) => this.setState(state);
        if(nextProps.typeId === DEPARTMENTS_TYPE || nextProps.typeId === POSITIONS_TYPE) {
            this.dispatcher.dispatch({typeId: newState0.typeId, action: 'loadAll'}, newState0, callback);
        }
    }

    handleCreateNew(title, description) {
        this.dispatcher.dispatch({
            action: 'addNew',
            typeId: this.state.typeId === DEPARTMENTS_TYPE ? DEPARTMENT_TYPE : POSITION_TYPE,
            data: {title, description}
        }, this.state, (state) => this.setState(state));
    }

    handleMoveTo(typeId, itemId) {
        this.setState(Object.assign({}, this.state, {typeId: typeId, itemId: itemId}));
    }

    getComponentByState() {
        switch (this.state.typeId) {
            case DEPARTMENTS_TYPE:
            case POSITIONS_TYPE:
                return (
                    <div className="row">
                        <ListComponent
                            typeId={this.state.typeId}
                            onClick={this.handleMoveTo}
                            data={this.state.data}
                        />
                        <div className="col-md-4">
                            <CreatePanel typeId={this.state.typeId} onSubmit={this.handleCreateNew}/>
                        </div>
                    </div>
                );
            case DEPARTMENT_TYPE:
                return <Department id={this.state.itemId} dispatcher={this.dispatcher} onClick={this.handleMoveTo}/>;
            case POSITION_TYPE:
                return <UpdateModelBlock id={this.state.itemId} typeId={this.state.typeId} dispatcher={this.dispatcher}/>;
            case EMPLOYEE_TYPE:
                return <Employee id={this.state.itemId} dispatcher={this.dispatcher} isUpdate={true}/>;
            case VACANCY_TYPE:
                return <Employee vacancyId={this.state.itemId} dispatcher={this.dispatcher} isUpdate={false}/>;
            default:
                return <div>Default Panel (Something wrong)</div>
        }
    }

    render() {
        return (
            <div className="container-fluid">
                {this.getComponentByState()}
            </div>
        );
    }
}

class ListComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            typeId: props.typeId,
            data: props.data
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({typeId: newProps.typeId, data: newProps.data});
    }

    render() {
        return (
            <div className="col-md-8">
                {this.state.data.map(item => {
                    return (<ListItem
                        key={`${this.state.typeId},${item.id}`}
                        id={item.id}
                        title={item.title}
                        description={item.description}
                        typeId={this.state.typeId}
                        onClick={this.props.onClick}
                    />)
                })}
            </div>
        );
    }

}

class ListItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            typeId: props.typeId,
            title: props.title,
            description: props.description
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState(Object.assign({}, this.state, newProps));
    }

    render() {
        let typeName = null;
        let newTypeId = null;
        switch (this.state.typeId) {
            case DEPARTMENT_TYPE:
            case DEPARTMENTS_TYPE: typeName = 'department'; newTypeId = DEPARTMENT_TYPE; break;
            case POSITION_TYPE:
            case POSITIONS_TYPE: typeName = 'position'; newTypeId = POSITION_TYPE; break;
            case VACANCY_TYPE:
            case VACANCIES_TYPE: typeName = 'vacancy'; newTypeId = VACANCY_TYPE; break;
            case EMPLOYEE_TYPE:
            case EMPLOYEES_TYPE: typeName = 'employee'; newTypeId = EMPLOYEE_TYPE; break;
            default: typeName = ''; newTypeId = this.state.typeId;
        }
        return (
            <div className="jumbotron">
                <div className="container">
                    <h4>
                        <a href={`#${typeName}=${this.state.id}`}
                           onClick={() => this.props.onClick(newTypeId, this.state.id)} role="button">
                            {this.state.title}
                        </a>
                    </h4>
                    <p>{this.state.description}</p>
                </div>
            </div>
        )
    }
}

class CreatePanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: ''
        };
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChangeTitle(event) {
        this.setState({
            title: event.target.value,
            description: this.state.description
        })
    }

    onChangeDescription(event) {
        this.setState({
            title: this.state.title,
            description: event.target.value,
        })
    }

    onSubmit() {
        this.props.onSubmit(this.state.title, this.state.description);
        this.setState({
            title: '',
            description: ''
        })
    }

    render() {
        return (
            <div className="container-fluid jumbotron">
                <form>
                    <div className="form-group">
                        <label>{this.props.typeId === 0? "department": "position"} name</label>
                        <input value={this.state.title} onChange={this.onChangeTitle}
                               className="form-control" required="true"/>
                    </div>
                    <div className="form-group">
                        <label>{this.props.typeId === 0? "department": "position"} description</label>
                        <textarea
                            value={this.state.description} onChange={this.onChangeDescription}
                            className="form-control" rows="5" required="true"/>
                    </div>
                    <button onClick={this.onSubmit} className="btn btn-info center-block">
                        Create New
                    </button>
                </form>
            </div>
        );
    }
}

class UpdateModelBlock extends React.Component {

    constructor(props) {
        super(props);
        this.dispatcher = props.dispatcher;
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            data: {
                title: '',
                description: ''
            },
            id: props.id,
            typeId: props.typeId
        }
    }

    componentWillMount() {
        const callback = (state) => {
            this.setState(state);
        };
        this.dispatcher.dispatch({
            action: 'get',
            typeId: this.state.typeId,
            itemId: this.state.id
        }, this.state, callback);
    }

    componentWillReceiveProps(nextProps) {
        const newState0 = Object.assign({}, this.state, {typeId: nextProps.typeId, id: nextProps.id});
        const callback = (state) => this.setState(state);
        this.dispatcher.dispatch({
            action: 'get',
            typeId: newState0.typeId,
            itemId: newState0.id
        }, newState0, callback);
    }

    onChangeTitle(event) {
        const newData = Object.assign({}, this.state.data, {title: event.target.value});
        this.setState(Object.assign({}, this.state, {data: newData}));
    }

    onChangeDescription(event) {
        const newData = Object.assign({}, this.state.data, {description: event.target.value});
        this.setState(Object.assign({}, this.state, {data: newData}));
    }

    onSubmit() {
        const callback = (state) => this.setState(state);
        this.dispatcher.dispatch({
            action: 'update',
            typeId: this.state.typeId,
            itemId: this.state.id,
            data: this.state.data
        }, this.state, callback);
    }

    render() {
        return (
            <div className="container-fluid center-block jumbotron">
                <label>{typeIdToStr(this.state.typeId)} id: {this.state.id}</label>
                <form>
                    <div className="form-group">
                        <label>{typeIdToStr(this.state.typeId)} name</label>
                        <input value={this.state.data.title} onChange={this.onChangeTitle}
                               className="form-control" required="true"/>
                    </div>
                    <div className="form-group">
                        <label>{typeIdToStr(this.state.typeId)} description</label>
                        <textarea value={this.state.data.description} onChange={this.onChangeDescription}
                                  className="form-control" rows="5" required="true"/>
                    </div>
                    <button onClick={this.onSubmit} className="btn btn-info center-block">
                        {typeIdToStr(this.state.typeId)} Update
                    </button>
                </form>
            </div>
        );
    }
}

class Department extends React.Component {

    constructor(props) {
        super(props);
        this.dispatcher = props.dispatcher;
        this.updateComponentState = this.updateComponentState.bind(this);
        this.getDepartmentUpdateBlock = this.getDepartmentUpdateBlock.bind(this);
        this.getCreateVacancyBlock = this.getCreateVacancyBlock.bind(this);
        this.getEmployeesBlock = this.getEmployeesBlock.bind(this);
        this.getVacanciesBlock = this.getVacanciesBlock.bind(this);
        this.onCreateVacancy = this.onCreateVacancy.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);
        this.state = {
            itemId: props.id,
            vacancies: [],
            employees: [],
            vacanciesFromDate: new Date(1, 1, 1, 0, 0, 0)
        }
    }

    updateComponentState() {
        const callback = (state) => this.setState(state);

        this.dispatcher.dispatch({
            action: 'get',
            typeId: DEPARTMENT_TYPE,
            itemId: this.state.itemId
        }, this.state, callback);

        this.dispatcher.dispatch({
            action: 'getAllById',
            typeId: VACANCIES_TYPE,
            itemId: this.state.itemId
        }, this.state, (vacancies) => {
            this.setState(Object.assign({}, this.state, {vacancies}))
        });

        this.dispatcher.dispatch({
            action: 'getAllById',
            typeId: EMPLOYEES_TYPE,
            itemId: this.state.itemId
        }, this.state, (employees) => {
            this.setState(Object.assign({}, this.state, {employees}))
        });
    }

    componentWillMount() {
        this.updateComponentState();
    }

    componentWillReceiveProps(newProps) {
        if (newProps.typeId === DEPARTMENT_TYPE) {
            this.setState(Object.assign({}, this.state, newProps));
            this.updateComponentState();
        } else {
            console.log(newProps);
        }
    }

    onCreateVacancy() {
        this.dispatcher.dispatch({
            action: 'getAll',
            typeId: VACANCIES_TYPE,
            itemId: this.state.itemId
        }, this.state, (newState) => {
            this.setState(Object.assign({}, this.state, {vacancies: newState.data}))
        });
    }

    getDepartmentUpdateBlock() {
        return (
            <div className="col-md-6">
                <UpdateModelBlock
                    id={this.state.itemId}
                    typeId={DEPARTMENT_TYPE}
                    dispatcher={this.dispatcher}/>
            </div>
        );
    }

    getCreateVacancyBlock() {
        return <CreateVacancyBlock
            dispatcher={this.dispatcher}
            departmentId={this.state.itemId}
            onCreate={this.onCreateVacancy}/>;
    }

    getEmployeesBlock() {
        const formattedEmployees = this.state.employees.map(e => {
            return {id: e.id, title: `${e.name_first} ${e.name_last} (${e.email})`, description: e.position.title}
        });
        return (
            <div className="col-md-6">
                <h3>Employees</h3>
                <ListComponent
                    typeId={EMPLOYEES_TYPE}
                    onClick={this.props.onClick}
                    data={formattedEmployees}
                />
            </div>
        );
    }

    getVacanciesBlock() {
        const formattedVacancies = this.state.vacancies.reduce((acc, v) => {
            if(new Date(v.date_start) > this.state.vacanciesFromDate){
                acc.push({id: v.id, title: v.title, description: `${v.position.title}. ${v.position.description}`});
            }
            return acc;
        }, []);
        return (
            <div className="col-md-6">
                <h3>Vacancies</h3>
                <label>start date</label>
                <Datetime onChange={this.onChangeDate} dateFormat='YYYY/MM/DD' timeFormat={false}/>
                <br/>
                <ListComponent
                    typeId={VACANCIES_TYPE}
                    onClick={this.props.onClick}
                    data={formattedVacancies}
                />
            </div>
        );
    }

    onChangeDate(date) {
        this.setState(Object.assign({}, this.state, {vacanciesFromDate: new Date(date.format())}));
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    {this.getDepartmentUpdateBlock()}
                    {this.getCreateVacancyBlock()}
                </div>
                <div className="row">
                    {this.getEmployeesBlock()}
                    {this.getVacanciesBlock()}
                </div>
            </div>
        );
    }
}

class CreateVacancyBlock extends React.Component {

    constructor(props) {
        super(props);
        this.dispatcher = props.dispatcher;
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangePosition = this.onChangePosition.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onCreate = props.onCreate;
        this.state = {
            departmentId: props.departmentId,
            positions: [],
            selectedPositionId: 1,
            title: '',
            dateStart: new Date()
        }
    }

    componentWillMount() {
        this.dispatcher.dispatch({
            action: 'getAll',
            typeId: POSITIONS_TYPE,
        }, this.state, (state) => {
            this.setState(Object.assign({}, this.state, {positions: state.data}));
        });
    }

    componentWillReceiveProps(newProps) {
        this.dispatcher.dispatch({
            action: 'getAll',
            typeId: POSITIONS_TYPE
        }, this.state, (state) => {
            this.setState(Object.assign({}, this.state, newProps, {selectedPositionId: 1}, {positions: state.data}));
        });
    }

    onChangeTitle(event) {
        this.setState(Object.assign({}, this.state, {title: event.target.value}));
    }

    onChangePosition(event) {
        this.setState(Object.assign({}, this.state, {selectedPositionId: event.target.value}));
    }

    onChangeDate(date) {
        this.setState(Object.assign({}, this.state, {dateStart: new Date(date.format())}));
    }

    onSubmit() {
        if(this.state.title && this.state.dateStart) {
            this.dispatcher.dispatch({
                action: 'addNew',
                typeId: VACANCY_TYPE,
                data: {
                    title: this.state.title,
                    dateStart: this.state.dateStart,
                    departmentId: this.state.departmentId,
                    positionId: this.state.selectedPositionId
                }
            }, this.state, (_state) => {
                const newState = Object.assign({}, this.state, {selectedPositionId: 1, title: '', dateStart: new Date()});
                this.setState(newState);
                this.onCreate();
            });
        }else {
            alert('all fields must be filled');
        }
    }

    render() {
        return (
            <div className="col-md-6 jumbotron">
                <h2>create vacancy</h2>
                <form>
                    <div className="form-group">
                        <label>vacancy title</label>
                        <input value={this.state.title} onChange={this.onChangeTitle}
                               className="form-control" required="true"/>
                    </div>
                    <div className="form-group">
                        <label>position type</label><br/>
                        <select value={this.state.selectedPositionId} onChange={this.onChangePosition}>
                            {this.state.positions.map(p => <option key={p.id} value={p.id}>{p.title}</option>)}
                        </select>
                    </div>
                    <div className="form-group">
                        <label>start date</label>
                        <Datetime onChange={this.onChangeDate} dateFormat='YYYY/MM/DD' timeFormat={false}/>
                    </div>
                    <button onClick={this.onSubmit} className="btn btn-info center-block">create vacancy</button>
                </form>
            </div>
        );
    }

}

class Employee extends React.Component {

    constructor(props) {
        super(props);
        this.dispatcher = props.dispatcher;
        this.onChangePosition = this.onChangePosition.bind(this);
        this.onChangeDepartment = this.onChangeDepartment.bind(this);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePhone = this.onChangePhone.bind(this);
        this.onCreate = this.onCreate.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.state = {
            employeeId: props.id,
            vacancyId: props.vacancyId,
            positionId: undefined,
            departmentId: undefined,
            positions: [],
            departments: [],
            firstName: '',
            lastName: '',
            email: '',
            phone: '',
            isUpdate: !!props.isUpdate
        }
    }

    componentWillMount() {
        this.dispatcher.dispatch({
            action: 'loadAll',
            typeId: POSITIONS_TYPE,
        }, this.state, (state) => {
            this.setState(Object.assign({}, this.state, {positions: state.data}));
        });
        if (this.state.isUpdate) {
            this.dispatcher.dispatch({
                action: 'loadAll',
                typeId: DEPARTMENTS_TYPE,
            }, this.state, (state) => {
                this.setState(Object.assign({}, this.state, {departments: state.data}));
            });
        }

        if(!this.state.vacancyId) {
            this.dispatcher.dispatch({
                action: 'get',
                typeId: EMPLOYEE_TYPE,
                itemId: this.state.employeeId
            }, this.state, (state) => {
                this.setState(Object.assign({}, this.state, {
                    positionId: state.data.position.id,
                    departmentId: state.data.departmentId,
                    firstName: state.data.name_first,
                    lastName: state.data.name_last,
                    email: state.data.email,
                    phone: state.data.phone
                }));
            });
        } else {
            this.dispatcher.dispatch({
                action: 'get',
                typeId: VACANCY_TYPE,
                itemId: this.state.vacancyId
            }, this.state, (state) => {
                this.setState(Object.assign({}, this.state, {
                    positionId: state.data.position.id,
                    departmentId: state.data.department_id
                }));
            });
        }
    }

    onChangePosition(event) {
        this.setState(Object.assign({}, this.state, {positionId: event.target.value}));
    }

    onChangeDepartment(event) {
        this.setState(Object.assign({}, this.state, {departmentId: event.target.value}));
    }

    onChangeFirstName(event) {
        this.setState(Object.assign({}, this.state, {firstName: event.target.value}));
    }

    onChangeLastName(event) {
        this.setState(Object.assign({}, this.state, {lastName: event.target.value}));
    }

    onChangeEmail(event) {
        this.setState(Object.assign({}, this.state, {email: event.target.value}));
    }

    onChangePhone(event) {
        this.setState(Object.assign({}, this.state, {phone: event.target.value}));
    }

    onUpdate() {
        console.log('update user');
    }

    onCreate() {
        const employee = {
            positionId: parseInt(this.state.positionId),
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            phone: this.state.phone,
            vacancyId: this.state.vacancyId
        };
        this.dispatcher.dispatch({
            action: 'addNew',
            typeId: EMPLOYEE_TYPE,
            data: employee
        }, this.state, (state) => {
            this.setState(state);
            alert(`created new employee, vacncy ${employee.vacancyId} was removed`);
        });
    }

    render() {
        return (
            <div className="center-block jumbotron">
                <h2>Employee</h2>
                <form>
                    <div className="form-group">
                        <label>First Name</label>
                        <input value={this.state.firstName}  onChange={this.onChangeFirstName}
                               className="form-control" required="true"/>
                    </div>
                    <div className="form-group">
                        <label>Last Name</label>
                        <input value={this.state.lastName} onChange={this.onChangeLastName}
                               className="form-control" required="true"/>
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input value={this.state.email} onChange={this.onChangeEmail}
                               className="form-control" required="true"/>
                    </div>
                    <div className="form-group">
                        <label>Phone</label>
                        <input value={this.state.phone} onChange={this.onChangePhone}
                               className="form-control" required="true"/>
                    </div>
                    <div className="form-group">
                        <label>Position</label>
                        <br/>
                        <select value={this.state.positionId} onChange={this.onChangePosition}>
                        {this.state.positions.map(p => <option key={p.id} value={p.id}>{p.title}</option>)}
                        </select>
                    </div>
                    <div className="form-group">
                        {this.state.isUpdate ? (
                            <div>
                                <label>Department</label>
                                <br/>
                                <select value={this.state.departmentId} onChange={this.onChangeDepartment}>
                                    {this.state.departments.map(d => <option key={d.id} value={d.id}>{d.title}</option>)}
                                </select>
                                <button onClick={this.onUpdate} className="btn btn-info center-block">Update</button>
                            </div>
                        ) : (
                            <button onClick={this.onCreate} className="btn btn-info center-block">Create</button>
                        )}
                    </div>
                </form>
            </div>
        );
    }
}
