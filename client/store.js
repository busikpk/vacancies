import {
    DEPARTMENTS_TYPE,
    DEPARTMENT_TYPE,
    POSITIONS_TYPE,
    POSITION_TYPE,
    EMPLOYEES_TYPE,
    EMPLOYEE_TYPE,
    VACANCIES_TYPE,
    VACANCY_TYPE
} from './constans';
import {typeIdToStr} from './utils';

export default class Dispatcher {

    constructor() {
        this.departments = new BaseListStore();
        this.positions = new BaseListStore();
        this.employees = new BaseListStore();
        this.vacancies = new BaseListStore();
    }

    getStore(typeId) {
        switch (typeId) {
            case DEPARTMENT_TYPE:
            case DEPARTMENTS_TYPE:
                return this.departments;
            case POSITION_TYPE:
            case POSITIONS_TYPE:
                return this.positions;
            case EMPLOYEE_TYPE:
            case EMPLOYEES_TYPE:
                return this.employees;
            case VACANCY_TYPE:
            case VACANCIES_TYPE:
                return this.vacancies;
            default:
                console.log('default store', typeId);
                return null;
        }
    }

    dispatch(request, oldState, callback) {
        this.matchBaseListAction(this.getStore(request.typeId), request, oldState, callback);
    }

    matchBaseListAction(store, request, oldState, callback) {
        switch (request.action) {
            case "getAll":
                this.getAll(store, request, oldState, callback);
                break;
            case "get":
                this.getOne(store, request, oldState, callback);
                break;
            case "getAllById":
                this.getAllById(store, request, oldState, callback);
                break;
            case "loadAll":
                this.loadAll(store, request, oldState, callback);
                break;
            case "loadOne":
                this.loadOne(store, request, oldState, callback);
                break;
            case "addNew":
                this.createNewItem(store, request, oldState, callback);
                break;
            case "update":
                this.updateOne(store, request, oldState, callback);
                break;
            default:
                console.log('default action for', request);
        }
    }

    getAll(store, request, oldState, callback) {
        if (store.data.length > 0) {
            callback(Object.assign({}, oldState, {data: store.data}));
        } else {
            this.matchBaseListAction(store, Object.assign({}, request, {action: "loadAll"}), oldState, callback);
        }
    }

    getOne(store, request, oldState, callback) {
        let target = null;
        store.data.forEach(item => target = request.itemId === item.id ? item : target);
        if (target) {
            callback(Object.assign({}, oldState, {data: target}));
        } else {
            this.matchBaseListAction(store, Object.assign({}, request, {action: 'loadOne'}), oldState, callback);
        }
    }

    loadAll(store, request, oldState, callback) {
        store.loadAll(request.typeId, () => callback(Object.assign({}, oldState, {data: store.data})));
    }

    loadOne(store, request, oldState, callback) {
        store.loadOne(request.typeId, request.itemId, () => {
            let target = null;
            store.data.forEach(item => target = request.itemId === item.id ? item : target);
            callback(Object.assign({}, oldState, {data: target}))
        });
    }

    createNewItem(store, request, oldState, callback) {
        store.createNewItem(request.typeId, request.data, () => {
            callback(Object.assign({}, oldState, {data: store.data}));
        });
    }

    updateOne(store, request, oldState, callback) {
        store.updateOne(request.typeId, request.data, () => {
            let target = null;
            store.data.forEach(item => target = request.data.id === item.id ? item : target);
            callback(Object.assign({}, oldState, {data: target}))
        });
    }

    getAllById(store, request, oldState, callback) {
        store.loadAllById(request.typeId, request.itemId, callback);
    }
}


class BaseListStore {

    constructor() {
        this.data = [];
        this.loadAllByIdPending = false;
        this.loadAllPending = false;
    }

    loadAllById(typeId, id, subscriber) {
        if (!this.loadAllByIdPending) {
            this.loadAllByIdPending = true;
            const type = typeIdToStr(typeId);
            fetch(`/api/${type}/${id}/`).then(resp => resp.json()).then(json => {
                this.data = json[type];
                subscriber(json[type]);
                this.loadAllByIdPending = false;
            }).catch(err => {
                console.log(err);
                this.loadAllByIdPending = false;
            })
        }
    }

    loadAll(typeId, subscriber) {
        if (!this.loadAllPending){
            this.loadAllPending = true;
            const type = typeIdToStr(typeId);
            fetch(`/api/${type}/`).then(resp => resp.json()).then(json => {
                this.data = json[type];
                subscriber();
                this.loadAllPending = false;
            }).catch(err => {
                console.log(err);
                this.loadAllPending = false;
            });
        }
    }

    createNewItem(typeId, body, subscriber) {
        const type = typeIdToStr(typeId);
        fetch(`/api/${type}/create/`, {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(body)
        }).then(resp => resp.json()).then(json => {
            if(json.errors.length > 0){
                alert(json.errors);
            }else {
                this.data = [json[type]].concat(this.data);
                subscriber();
            }
        }).catch(err => console.log(err));
    }

    loadOne(typeId, id, subscriber) {
        const type = typeIdToStr(typeId);
        fetch(`/api/${type}/${id}/`).then(resp => resp.json()).then(json => {
            this.data.push(json[type]);
            subscriber();
        });
    }

    updateOne(typeId, body, subscriber) {
        const type = this.typeId === DEPARTMENTS_TYPE ? 'department' : 'position';
        fetch(`/api/${type}/update/`,{
            method: 'patch',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(body)
        }).then(resp => resp.json()).then(json => {
            if(json.errors.length > 0) {
                alert(json.errors);
            }else {
                this.data = this.data.map(item => {
                    if (item.id === body.id) {
                        return json[type];
                    } else {
                        return item;
                    }
                });
                subscriber();
                alert('updated');
            }
        });
    }
}
