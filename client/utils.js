import {
    DEPARTMENTS_TYPE,
    POSITIONS_TYPE,
    DEPARTMENT_TYPE,
    POSITION_TYPE,
    VACANCY_TYPE,
    EMPLOYEE_TYPE,
    EMPLOYEES_TYPE, VACANCIES_TYPE
} from './constans';

function typeIdToStr(typeId) {
    switch (typeId) {
        case DEPARTMENTS_TYPE:
            return 'departments';
        case DEPARTMENT_TYPE:
            return 'department';
        case POSITIONS_TYPE:
            return 'positions';
        case POSITION_TYPE:
            return 'position';
        case VACANCIES_TYPE:
            return 'vacancies';
        case VACANCY_TYPE:
            return 'vacancy';
        case EMPLOYEES_TYPE:
            return 'employees';
        case EMPLOYEE_TYPE:
            return 'employee';
        default:
            console.log(typeId);
            return '';
    }
}

function getCurrentPanelConfigByHash() {
    const hash = location.hash.substr(1);
    let [typeName = 'departments', id = null] = hash.split('=');
    if(id) {
        switch (typeName) {
            case 'department': return {typeId: DEPARTMENT_TYPE, itemId: parseInt(id)};
            case 'position': return {typeId: POSITION_TYPE, itemId: parseInt(id)};
            case 'employee': return {typeId: EMPLOYEE_TYPE, itemId: parseInt(id)};
            case 'vacancy': return {typeId: VACANCY_TYPE, itemId: parseInt(id)};
            default: return {typeId: -1, itemId: null};
        }
    } else {
        if(typeName === 'positions') {
            return {typeId: POSITIONS_TYPE, itemId: null};
        } else {
            return {typeId: DEPARTMENTS_TYPE, itemId: null};
        }
    }
}



export {
    typeIdToStr,
    getCurrentPanelConfigByHash
}