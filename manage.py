from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from server import app
from server.models import db

migrate = Migrate(app, db, directory='server/migrations')
manager = Manager(app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
