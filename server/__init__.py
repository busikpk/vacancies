import json

from flask import Flask

from .models import db
from .schems import ma
from .routers import setup_routes

with open('config.json') as data_file:
    config = json.load(data_file)

app = Flask(__name__, template_folder='templates')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config['SQLALCHEMY_TRACK_MODIFICATIONS']
app.config['SQLALCHEMY_DATABASE_URI'] = config['SQLALCHEMY_DATABASE_URI']

db.init_app(app)
ma.init_app(app)
setup_routes(app)


