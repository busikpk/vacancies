from flask import request, jsonify
from sqlalchemy.exc import IntegrityError

from .models import *
from .schems import *


def get_departments():
    departments_decoded = DepartmentSchema().dump(Department.query.order_by(Department.id).all(), many=True).data
    return jsonify({'departments': departments_decoded})


def get_department(department_id):
    department = Department.query.get(department_id)
    department_decoded = DepartmentSchema().dump(department).data
    return jsonify({'department': department_decoded})


def get_position(position_id):
    position = Position.query.get(position_id)
    position_decoded = PositionSchema().dump(position).data
    return jsonify({'position': position_decoded})


def get_positions():
    positions_decoded = PositionSchema().dump(Position.query.order_by(Position.id).all(), many=True).data
    return jsonify({'positions': positions_decoded})


def get_vacancies(department_id):
    vacancies = Vacancy.query.filter_by(department_id=department_id).order_by(Vacancy.id).all()
    vacancies_decoded = VacancySchema().dump(vacancies, many=True).data
    return jsonify({'vacancies': vacancies_decoded})


def get_vacancy(vacancy_id):
    vacancy = Vacancy.query.get(vacancy_id)
    vacancy_decoded = VacancySchema().dump(vacancy).data
    return jsonify({'vacancy': vacancy_decoded})


def get_employees(department_id):
    employees = Employee.query.filter_by(department_id=department_id).all()
    employees_decoded = EmployeeSchema().dump(employees, many=True).data
    return jsonify({'employees': employees_decoded})


def get_employee(employee_id):
    employee = Employee.query.get(employee_id)
    employee_decoded = EmployeeSchema().dump(employee).data
    return jsonify({'employee': employee_decoded})


def get_employees_history(employee_id):
    employee_history = EmployeeHistory.query.filter_by(employee_id=employee_id).all()
    employee_history_decoded = EmployeeHistorySchema().dump(employee_history, many=True).data
    return jsonify({'employee_history': employee_history_decoded})


def add_department():
    department_json = request.get_json()
    if department_json['title'] and department_json['description']:
        try:
            department = Department(department_json['title'], department_json['description'])
            db.session.add(department)
            db.session.commit()
            department_json['id'] = department.id
        except IntegrityError:
            return jsonify({'errors': ['duplicate department title'], 'department': None})

        return jsonify({'errors': [], 'department': department_json})

    return jsonify({'errors': ['bad input data'], 'department': None})


def add_position():
    position_json = request.get_json()
    if position_json['title'] and position_json['description']:
        try:
            position = Position(position_json['title'], position_json['description'])
            db.session.add(position)
            db.session.commit()
            position_json['id'] = position.id
        except IntegrityError:
            return jsonify({'errors': ['duplicate position title'], 'position': None})

        return jsonify({'errors': [], 'position': position_json})

    return jsonify({'errors': ['bad input data'], 'position': None})


def add_vacancy():
    vacancy_json = request.get_json()
    if all([vacancy_json.get(key, None) for key in ['title', 'dateStart', 'departmentId', 'positionId']]):
        try:
            vacancy = Vacancy(vacancy_json['title'], vacancy_json['departmentId'],
                              vacancy_json['positionId'], vacancy_json['dateStart'])
            db.session.add(vacancy)
            db.session.commit()
            return jsonify({'errors': [], 'vacancy': VacancySchema().dump(vacancy).data})
        except IntegrityError as err:
            print(err)
            return jsonify({'errors': ['bad input data'], 'vacancy': None})

    return jsonify({'errors': ['not all required fields send'], 'vacancy': None})


def add_employee():
    employee_json = request.get_json()
    print(employee_json)
    required_fields = ['email', 'firstName', 'lastName', 'phone', 'positionId', 'vacancyId']
    if all([employee_json.get(key, None) for key in required_fields]):
        try:
            vacancy = Vacancy.query.get(employee_json['vacancyId'])
            if vacancy:
                employee = Employee(employee_json['email'], employee_json['phone'], employee_json['firstName'],
                                    employee_json['lastName'], employee_json['positionId'], vacancy.department_id)
                db.session.add(employee)
                db.session.delete(vacancy)
                db.session.commit()
                return jsonify({'errors': [], 'employee': EmployeeSchema().dump(employee).data})
            else:
                return jsonify({'errors': ['vacancy 404'], 'employee': None})
        except IntegrityError as err:
            print(err)
            return jsonify({'errors': ['bad input data'], 'employee': None})

    return jsonify({'errors': ['not all required fields send'], 'employee': None})


def update_department():
    department_new = request.get_json()
    if department_new['id']:
        try:
            department_old = Department.query.get(department_new['id'])
            department_old.title = department_new['title']
            department_old.description = department_new['description']
            db.session.commit()
        except IntegrityError:
            return jsonify({'errors': ['bad department object'], 'department': None})

        return jsonify({'errors': [], 'department': department_new})

    return jsonify({'errors': ['id not found'], 'department': None})


def update_position():
    position_new = request.get_json()
    if position_new['id']:
        try:
            position_old = Position.query.get(position_new['id'])
            position_old.title = position_new['title']
            position_old.description = position_new['description']
            db.session.commit()
        except IntegrityError:
            return jsonify({'errors': ['bad position object'], 'position': None})

        return jsonify({'errors': [], 'position': position_new})

    return jsonify({'errors': ['id not found'], 'position': None})
