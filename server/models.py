from datetime import datetime

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Department(db.Model):
    __tablename__ = 'department'
    id = db.Column(db.Integer, db.Sequence('department_id_seq'), primary_key=True)
    title = db.Column(db.String(256), unique=True, nullable=False)
    description = db.Column(db.Text, nullable=False)

    def __init__(self, title, description):
        self.title = title
        self.description = description

    def __repr__(self):
        return 'Department: [id: {}, title: {}, description: {}]'.format(self.id, self.title, self.description)


class Vacancy(db.Model):
    __tablename__ = 'vacancy'
    id = db.Column(db.Integer, db.Sequence('vacancy_id_seq'), primary_key=True)
    title = db.Column(db.String(256), nullable=False)
    date_start = db.Column(db.DateTime, nullable=False)
    date_end = db.Column(db.DateTime)
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'), nullable=False)
    position_id = db.Column(db.Integer, db.ForeignKey('position.id'), nullable=False)
    position = db.relationship('Position', uselist=False, lazy='joined')

    def __init__(self, tittle, department_id, position_id, date_start=None):
        self.title = tittle
        self.department_id = department_id
        self.position_id = position_id
        if date_start is None:
            date_start = datetime.utcnow()
        self.date_start = date_start

    def __repr__(self):
        return 'Vacancy:[id:{},title:{},department_id:{},date_start:{},date_end: {}]'.format(
               self.id, self.title, self.department_id, self.date_start, self.date_end)


class Employee(db.Model):
    __tablename__ = 'employee'
    id = db.Column(db.Integer, db.Sequence('employee_id_seq'), primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    phone = db.Column(db.String(256), unique=True, nullable=False)
    name_first = db.Column(db.String(128), nullable=False)
    name_last = db.Column(db.String(128), nullable=False)
    date_birth = db.Column(db.DateTime, nullable=False)
    date_start = db.Column(db.DateTime, nullable=False)
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'), nullable=False)
    position_id = db.Column(db.Integer, db.ForeignKey('position.id'), nullable=False)
    position = db.relationship('Position', uselist=False, lazy='joined')

    def __init__(self, email, phone, name_first, name_last, position_id, department_id, date_start=None, date_birth=None):
        self.email = email
        self.phone = phone
        self.name_first = name_first
        self.name_last = name_last
        self.date_birth = datetime.utcnow() if date_birth is None else date_birth
        self.date_start = datetime.utcnow() if date_start is None else date_start
        self.position_id = position_id
        self.department_id = department_id

    def __repr__(self):
        return 'Employee:[id:{},email:{},phone:{},position_id:{},department_id:{}]'.format(
                self.id, self.email, self.phone, self.position_id, self.department_id)


class EmployeeHistory(db.Model):
    __tablename__ = 'employee_history'
    id = db.Column(db.Integer, db.Sequence('employee_history_id_seq'), primary_key=True)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'), nullable=False)
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'), nullable=False)
    position_id = db.Column(db.Integer, db.ForeignKey('position.id'), nullable=False)
    position = db.relationship('Position', uselist=False, lazy='joined')
    department = db.relationship('Department', uselist=False, lazy='joined')

    def __init__(self, employee_id, department_id, position_id):
        self.employee_id = employee_id
        self.department_id = department_id
        self.position_id = position_id

    def __repr__(self):
        return 'EmployeeHistory:[id:{},employee_id:{},department_id:{},position_id:{}]'.format(
                self.id, self.employee_id, self.department_id, self.position_id)


class Position(db.Model):
    __tablename__ = 'position'
    id = db.Column(db.Integer, db.Sequence('position_id_seq'), primary_key=True)
    title = db.Column(db.String(128), unique=True, nullable=False)
    description = db.Column(db.Text, nullable=False)

    def __init__(self, title, description):
        self.title = title
        self.description = description

    def __repr__(self):
        return 'Position:[id:{},title:{},description:{}]'.format(self.id, self.title, self.description)

