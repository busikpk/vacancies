from .views import index
from .api import *


def setup_routes(app):
    app.add_url_rule('/', view_func=index)

    app.add_url_rule('/api/departments/', methods=['GET'], view_func=get_departments)
    app.add_url_rule('/api/department/<int:department_id>/', methods=['GET'], view_func=get_department)
    app.add_url_rule('/api/positions/', methods=['GET'], view_func=get_positions)
    app.add_url_rule('/api/position/<int:position_id>/', methods=['GET'], view_func=get_position)
    app.add_url_rule('/api/vacancies/<int:department_id>/', methods=['GET'], view_func=get_vacancies)
    app.add_url_rule('/api/vacancy/<int:vacancy_id>/', methods=['GET'], view_func=get_vacancy)
    app.add_url_rule('/api/employees/<int:department_id>/', methods=['GET'], view_func=get_employees)
    app.add_url_rule('/api/employee/<int:employee_id>/', methods=['GET'], view_func=get_employee)
    app.add_url_rule('/api/employee_history/<int:employee_id>/', methods=['GET'], view_func=get_employees_history)

    app.add_url_rule('/api/department/create/', methods=['POST'], view_func=add_department)
    app.add_url_rule('/api/position/create/', methods=['POST'], view_func=add_position)
    app.add_url_rule('/api/vacancy/create/', methods=['POST'], view_func=add_vacancy)
    app.add_url_rule('/api/employee/create/', methods=['POST'], view_func=add_employee)

    app.add_url_rule('/api/department/update/', methods=['PATCH'], view_func=update_department)
    app.add_url_rule('/api/position/update/', methods=['PATCH'], view_func=update_position)
