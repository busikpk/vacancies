from flask_marshmallow import Marshmallow

ma = Marshmallow()


class DepartmentSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'description')


class PositionSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'description')


class VacancySchema(ma.Schema):
    class Meta:
       fields = ('id', 'title', 'date_start', 'date_end', 'department_id', 'position')

    position = ma.Nested(PositionSchema)


class EmployeeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'email', 'phone', 'name_first', 'name_last',
                  'date_birth', 'date_start', 'department_id', 'position')

    position = ma.Nested(PositionSchema)


class EmployeeHistorySchema(ma.Schema):
    class Meta:
        fields = ('id', 'employee_id', 'department', 'position')

    department = ma.Nested(DepartmentSchema)
    position = ma.Nested(PositionSchema)
