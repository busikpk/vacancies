'use strict';

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    entry: "./client/index",
    output: {
        filename: "server/static/bundle.js"
    },

    // watch: true,
    //
    // watchOptions: {
    //     aggregateTimeout: 500
    // },

    module: {
        loaders: [
            {
                test: /(\.js$|\.jsx$)/,
                use: 'babel-loader',
                exclude: [/node_modules/]
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: "css-loader"
                })
            }
        ]
    },

    plugins: [
        new ExtractTextPlugin("server/static/styles.css"),
        new webpack.DefinePlugin({
            'process.env': {'NODE_ENV': JSON.stringify('production')}
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            comments: false,
            compress: {
                sequences: true,
                booleans: true,
                loops: true,
                unused: true,
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    ]
};
